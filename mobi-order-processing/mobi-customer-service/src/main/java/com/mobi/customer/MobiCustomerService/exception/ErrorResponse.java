package com.mobi.customer.MobiCustomerService.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "error")
public class ErrorResponse {

  private String message;

  private List<String> details;

}
